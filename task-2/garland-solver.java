package week6;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class garlandSolver {

    BufferedReader bufferedReader;
    BufferedWriter bufferedWriter;

    private double dichotomyMethod(int n, double A) {
        double[] h = new double[n];
        h[0] = A;

        double left = 0;
        double right = A;
        while (right - left > 0.0000000001) {
            h[1] = (left + right) / 2;
            boolean isLegal = true;
            for (int i = 2; i < n; i++) {
                h[i] = 2 * h[i - 1] - h[i - 2] + 2;
                if (h[i] < 0) {
                    isLegal = false;
                    break;
                }
            }
            if (isLegal) {
                right = h[1];
            } else {
                left = h[1];
            }
        }
        return h[n - 1];
    }


    private void solve() throws IOException {
        String[] ans = bufferedReader.readLine().split(" ");
        int n = Integer.parseInt(ans[0]);
        double A = Double.parseDouble(ans[1]);

        double result = dichotomyMethod(n, A);
        double trunkedResult = BigDecimal.valueOf(result)
                        .setScale(6, RoundingMode.HALF_EVEN)
                                .doubleValue();
        bufferedWriter.write(String.valueOf(trunkedResult));
    }

    private void run() {
        try {
            bufferedReader = new BufferedReader(new FileReader(new File("input.txt")));
            bufferedWriter = new BufferedWriter(new FileWriter(new File("output.txt")));

            solve();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bufferedReader.close();
                bufferedWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        new garlandSolver().run();
    }
}
