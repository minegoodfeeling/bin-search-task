package week6;

import java.io.*;

public class binSearchSolver {

    BufferedReader bufferedReader;
    BufferedWriter bufferedWriter;

    private int binSearch(int[] array, int x) {
        int left = -1;
        int right = array.length;

        while (right - left > 1) {
            int mid = (right - left) / 2 + left;
            if (array[mid] < x) {
                left = mid;
            } else {
                right = mid;
            }
        }
        if (right < array.length && array[right] == x) {
            return right;
        } else return -1;
    }

    private void solve() throws IOException {
        int n = Integer.parseInt(bufferedReader.readLine());
        int[] mainArray = new int[n];

        String[] arrayStr = bufferedReader.readLine().split(" ");

        for (int i = 0; i < n; i++) {
            mainArray[i] = Integer.parseInt(arrayStr[i]);
        }

        int m = Integer.parseInt(bufferedReader.readLine());
        String[] searchStr = bufferedReader.readLine().split(" ");

        for (int i = 0; i < m; i++) {
            int searchingNumber = Integer.parseInt(searchStr[i]);
            int leftIndex = binSearch(mainArray, searchingNumber);
            int rightIndex = leftIndex;

            if (leftIndex == -1) {
                bufferedWriter.write("-1 -1\n");
            } else {
                while (rightIndex < n && mainArray[rightIndex] == searchingNumber) {
                    rightIndex++;
                }
                bufferedWriter.write(++leftIndex + " " + rightIndex + "\n");
            }

        }

    }


    private void run() {
        try {
            bufferedReader = new BufferedReader(new FileReader(new File("input.txt")));
            bufferedWriter = new BufferedWriter(new FileWriter(new File("output.txt")));

            solve();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bufferedReader.close();
                bufferedWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        new binSearchSolver().run();
    }
}
